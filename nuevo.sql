create database db_emplead0s1;
use db_emplead0s1;

create table area_empresa(
folio_area int auto_increment,
area varchar(80),
constraint primary key(folio_area)
);

create table puesto(
folio_puesto int auto_increment,
puesto varchar(81),
constraint primary key(folio_puesto)
);

create table datos_personales(
folio_trabajador int auto_increment,
nombre varchar(81),
a_paterno varchar(81),
a_materno varchar(81),
sexo varchar(81),
direccion varchar(81),
edad int(12),
telefono varchar(12),
area_empresa int,
puesto_empresa int,
sueldo double,
constraint primary key(folio_trabajador)
);
alter table datos_personales add constraint foreign key(area_empresa) references area_empresa(folio_area) on delete cascade on update cascade;
alter table datos_personales add constraint foreign key(puesto_empresa) references puesto(folio_puesto) on delete cascade on update cascade;



create table prestaciones(
folio_prestaciones int auto_increment,
folio_personal int,
vales double,
ayud double,
aguinaldo double,
primav double,
imss double,
ISR double,
comedor double,
constraint primary key(folio_prestaciones)
);

alter table prestaciones add constraint foreign key(folio_personal) references datos_personales(folio_trabajador);
#Registro puesto
insert into puesto(folio_puesto, puesto)
values(1, 'director ejecutivo'),
(2, 'director de operaciones'),
(3, 'director de comercio'),
(4, 'director de recursos humanos'),
(5, 'director de finanzas'),
(6, 'empleado'),
(7, 'sistemas');

#Registros area
insert into area_empresa (folio_area,area)
values(1, 'director general'),
(2, 'auxiliar administrativo'),
(3, 'administracion y recursos humanos'),
(4, 'finanzas y contabilidad'),
(5, 'publicidad y mercadotecnia'),
(6, 'informatica');

#Registros datos
insert into datos_personales 
values(1, 'daniel alejandro', 'flores', 'islas', 'hombre', 'calle olimpica', 21, '55-1234-5678', 6, 1,10000);

desc prestaciones;
#Registros prestaciones


insert into prestaciones 
values (null,null,3000,null,null,null,null,null,null);

select*from datos_personales;

select*from prestaciones;

show tables;